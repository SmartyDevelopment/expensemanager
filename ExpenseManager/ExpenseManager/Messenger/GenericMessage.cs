﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ExpenseManager.Messenger
{
    internal class GerericMessage<T>
    {
        private int frameid;
        private string name;

        public GerericMessage(int frameid)
        {
            this.frameid = frameid;
        }

        public GerericMessage(string name)
        {
            this.name = name;
        }

    }
}
